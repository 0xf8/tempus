/*

Copyright 2022-2023 0xf8.dev@proton.me

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 

*/


#ifndef __MAIN_H__
#define __MAIN_H__

#include <locale.h>
#include <memory.h>
// #include <ncurses.h>
#include <notcurses/notcurses.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

typedef unsigned long size;
typedef unsigned int uint;

struct string {
  char *s;
  size  length;
};

struct bignumber_t {
  uint matrix[5][3];
  uint mimick;
};


struct string *alloc_string(uint l);
void free_string(struct string *s);

// pads a number with 0's
struct string *pad_int(int n);

// this creates a bignumber_t
struct bignumber_t *bignumber(int start, uint mimick);

// this draws a big number
void drawbignumber(struct bignumber_t *, int sec, int startx, int starty, struct ncplane *place);

#endif // __MAIN_H__
