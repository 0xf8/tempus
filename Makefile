OBJ     = $(patsubst src/%.c,%.o,$(wildcard src/*.c))
HEADERS = $(wildcard src/*.h)
CC      = clang
FLAGS   = -std=c17 -O3 -pthread
LIB     = -lncurses -lnotcurses -lnotcurses-core

%.o: src/%.c $(HEADERS)
	$(CC) $(FLAGS) -c $< -o $@

tempus: $(OBJ)
	$(CC) $(LIB) $(FLAGS) $^ -o $@


.PHONY: clean
clean:
	rm $(OBJ)