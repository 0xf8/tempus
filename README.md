# Tempus - Time
A nice little clock app.

## Reading the clock
- Left side of the `|` is the hour
- Right side of the `|` is the minute
- The highlighted number is the second

## Dependencies
- ncurses
- clang
- make

## Compiling
`make`

## Running
`./tempus`

## License
This program is released under the GPL3.0 license. This is available at https://www.gnu.org/licenses/gpl-3.0.html

## Copyright
Copyright 2022-2023 0xf8.dev@proton.me